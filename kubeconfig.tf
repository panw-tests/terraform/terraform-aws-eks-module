# Configure Kubectl
data "template_file" "kubeconfig" {
  template = file(var.kubeconfig_template_path)

  vars = {
    kubeconfig_name     = "eks_${aws_eks_cluster.eks_cluster.name}"
    clustername         = aws_eks_cluster.eks_cluster.name
    endpoint            = aws_eks_cluster.eks_cluster.endpoint
    cluster_auth_base64 = aws_eks_cluster.eks_cluster.certificate_authority[0].data
  }
}